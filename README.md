# f1-push-notifications-cli

## Поддерживаемые платформы

- android (firebase)

## Установка

```
npm i
cp .env.example .env
```

добавить PUSH_API_IP и FCM_KEY

## Пример использование 

```
npm run cli -- push one --platform android --token <device_token> --title world --message hello 
```