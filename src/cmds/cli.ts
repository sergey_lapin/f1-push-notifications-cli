#!/usr/bin/env -S node_modules/.bin/tsx

import { runAndLog } from "../utils";
import { pushCmds } from "./push";

(async () => {
  await runAndLog({
    name: "cli",
    cmds: {
      ...pushCmds({}),
    },
  });
})();
