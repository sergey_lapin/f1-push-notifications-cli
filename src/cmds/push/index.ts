import { subcommands } from "cmd-ts";

import { pushOne } from "./one";
import { pushMany } from "./many";

export const pushCmds = ({}) => ({
  push: subcommands({
    name: "push",
    cmds: {
      one: pushOne({}),
      many: pushMany({}),
    },
  }),
});
