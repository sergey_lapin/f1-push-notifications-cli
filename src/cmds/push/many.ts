import { command } from "cmd-ts";

import { PlatformT, pushManyArgs } from "../../options";
import { pushOneHandler } from "./one";
import { flattenTablesMap } from "../../utils";

const pushManyHandler =
  ({}: {}) =>
  async ({ text }: { text: string }) =>
    flattenTablesMap({
      tables: [PlatformT.android],
      func: async (platform: PlatformT) =>
        await pushOneHandler({})({
          platform,
          text,
        }),
    });

export const pushMany = ({}) =>
  command({
    name: "platformMany",
    args: pushManyArgs,
    handler: pushManyHandler({}),
  });
