import { command } from "cmd-ts";
import axios from "axios";

import curlirize from 'axios-curlirize';

curlirize(axios);

axios.defaults.baseURL = `http://${PUSH_API_IP}`;

import { PlatformT, pushOneArgs } from "../../options";
import { PUSH_API_IP, FCM_KEY } from "../..";

export const pushOneHandler =
  ({}: {}) =>
  async ({ platform, title, message, token }: { platform: string; title: string, message: string, token: string }) => {
    const tPlatform = platform as PlatformT;

    return await axios
      .post("/api/push", {
        "notifications": [
          {
            "tokens": [token],
            "platform": 2,
            message,
            title,
            "api_key": FCM_KEY
          }
        ]
      })
      
  };

export const pushOne = ({}) =>
  command({
    name: "pushOne",
    args: pushOneArgs,
    handler: pushOneHandler({}),
  });
