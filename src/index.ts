import { load } from "ts-dotenv";

export const { PUSH_API_IP, FCM_KEY } = load({
  PUSH_API_IP: String,
  FCM_KEY: String
});
