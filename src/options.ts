import { option, oneOf, multioption, array, string } from "cmd-ts";

export enum PlatformT {
  android = "android",
}

const message = {
  message: option({
    type: string,
    long: "message",
  }),
};

const title = {
  title: option({
    type: string,
    long: "title",
  }),
};


const token = {
  token: option({
    type: string,
    long: "token",
  }),
};

export const pushOneArgs = {
  platform: option({
    type: oneOf(Object.keys(PlatformT)),
    long: "platform",
  }),
  ...message,
  ...title,
  ...token,
};

export const pushManyArgs = {
  platform: multioption({
    type: array(oneOf(Object.values(PlatformT))),
    long: "platform",
  }),
  ...message,
  ...title,
  ...token,
};
