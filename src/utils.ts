import { flatten } from "lodash";
import { run, subcommands } from "cmd-ts";

import Logger from "@ptkdev/logger";

export const logger = new Logger();

export const runAndLog = async ({
  name,
  cmds,
}: {
  name: string;
  cmds: any;
}) => {
  const { value } = await run(
    subcommands({
      name,
      cmds,
    }),
    process.argv.slice(2),
  );

  console.log((value as any).value.data);
};

export const flattenTablesMap = async <T>({
  tables,
  func,
  seq,
}: {
  tables: T[];
  func: any;
  seq?: boolean;
}) => {
  if (seq) {
    let result = [];
    for (const table of tables) {
      result.push(await func(table));
    }
    return flatten(result);
  }

  return flatten(await Promise.all(tables.map(func)));
};
